///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file long.c
/// @version 1.0
///
/// Print the characteristics of the "long", "signed long" and "unsigned long" datatypes.
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   1/31/21
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "long.h"


///////////////////////////////////////////////////////////////////////////////
/// long

/// Print the characteristics of the "long" datatype
void doLong() {
   printf(TABLE_FORMAT_LONG, "long", sizeof(long)*8, sizeof(long), LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "long" datatype
void flowLong() {
   long overflow = LONG_MAX;
   printf("long overflow: %ld + 1 ", overflow++);
   printf("becomes %ld\n", overflow);
   
   long underflow = LONG_MIN;
   printf("long underflow: %ld - 1 ", underflow--);
   printf("becomes %ld\n", underflow);

}


///////////////////////////////////////////////////////////////////////////////
/// unsigned long

/// Print the characteristics of the "unsigned long" datatype
void doUnsignedLong() {
    printf(TABLE_FORMAT_ULONG, "unsigned long", sizeof(unsigned long)*8, sizeof(unsigned long), 0, ULONG_MAX);
}

/// Print the overflow/underflow characteristics of the "unsigned long" datatype
void flowUnsignedLong() {
   unsigned long overflow = ULONG_MAX;
   printf("unsigned long overflow: %lu + 1 ",  (unsigned long) overflow++);
   printf("becomes %lu\n", (unsigned long) overflow);
   
   unsigned long underflow = 0;
   printf("unsigned long underflow: %lu - 1 ", (unsigned long) underflow--);
   printf("becomes %lu\n", (unsigned long) underflow);
}

