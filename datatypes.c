///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// Usage:  datatypes
///
/// Result:
///   Print out platform datatype information from <limits.h> 
///
/// Example:
///   $ datatypes
///  XXXXXXXXXX
///
/// @file datatypes.c
/// @version 1.0
///
/// @warning This program must be compiled on the platform it's exploring.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "datatypes.h"
#include "char.h"
#include "short.h"

#include"int.h"
#include"long.h"

int main(int argc, char* argv[]) {
	printf("Platform C Datatype explorer\n");
   printf("\n");
   printf("This program must be compiled on the platform it's exploring.  Can you guess why?\n");
   printf("\n");
   printf("This program [%s] was compiled on [%s] at [%s] [%s]\n", argv[0], HOST, __DATE__, __TIME__);

#ifdef __INTEL_COMPILER
   printf("The Intel C++ compiler was used for compiling this program\n");
#elif __GNUC__
   printf("The GNU C Compiler version [%s] was used for compiling this program\n", __VERSION__);
#elif _MSC_VER
   printf("The compiler is Microsoft Visual Studio version [%d]\n", _MSC_VER);
#else
   printf("Can not determine the compiler\n");
#endif

   printf ("\n");
   printf (TABLE_HEADER1);
   printf (TABLE_HEADER2);

   doChar();
   doSignedChar();
   doUnsignedChar();
   doShort();           /// @todo:  Currently stubbed out
   doUnsignedShort();   /// @todo:  Currently stubbed out

   doInt();
   doUnsignedInt();
   doLong();
   doUnsignedLong();

   printf ("\n");

   flowChar();
   flowSignedChar();
   flowUnsignedChar();
   flowShort();         /// @todo:  Currently stubbed out
   flowUnsignedShort(); /// @todo:  Currently stubbed out

   flowInt();
   flowUnsignedInt();
   flowLong();
   flowUnsignedLong();

   return 0;
}

