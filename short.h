///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.h
/// @version 1.0
///
/// Print the characteristics of the "short" and "unsigned short" datatypes.
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   1/31/21
///////////////////////////////////////////////////////////////////////////////

extern void doShort();            /// Print the characteristics of the "short" datatype
extern void flowShort();          /// Print the overflow/underflow characteristics of the "short" datatype

extern void doSignedShort();      /// Print the characteristics of the "signed short" datatype
extern void flowSignedShort();    /// Print the overflow/underflow characteristics of the "signed short" datatype

extern void doUnsignedShort();    /// Print the characteristics of the "unsigned short" datatype
extern void flowUnsignedShort();  /// Print the overflow/underflow characteristics of the "unsigned short" datatype

